package test;

import lt.vsc.karolis.coffeeMachine.cupHierarchy.BigCoffeeCup;
import lt.vsc.karolis.coffeeMachine.cupHierarchy.CoffeeRecepies;
import lt.vsc.karolis.coffeeMachine.cupHierarchy.Cup;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import lt.vsc.karolis.coffeeMachine.CoffeMashine;

public class CoffeMashineTest {
    @Test
    public void  turimKavosAparataUzpildyta_turiGrazintiPuodeliSuPakeistareiksme(){
        Cup cup = new Cup();
        CoffeMashine coffeMashine = new CoffeMashine(500,500,500);
        Cup cup2 = coffeMashine.makeCoffe(1,cup);
        Assertions.assertTrue(cup2.isCoffeeMade);
    }
    @Test
    public void kavosAparatuiGaminantKavaIrNaudojantRecepta_truiMazetiProduktaiKavosAparate(){
        //Cup cup = new Cup();
        BigCoffeeCup cup = new BigCoffeeCup();
        CoffeMashine coffeMashine = new CoffeMashine(500,500,500);
        CoffeeRecepies recepie = CoffeeRecepies.pickRecepieByNum(1);
        coffeMashine.makeCoffe(1,cup);
        Assertions.assertTrue((coffeMashine.products.sugar-=recepie.sugar)==420);
    }
}
