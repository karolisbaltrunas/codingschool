package test;

import main.testexample.KavosMasina;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class KavosMasinaTest {
    @Test
    public void duotasUzpildytasKavosAparatas_kaiGaminamKavaTadaPamazeja(){
        //Duota
        KavosMasina kavosMasina = new KavosMasina();
        kavosMasina.uzpildytiPupeles(100);
        kavosMasina.uzpildytiVandens(2);
        //Kai
        kavosMasina.darytiJuodaKava();
        //Tada
        Assertions.assertTrue(kavosMasina.getVandensKiekis()==1);
        Assertions.assertTrue(kavosMasina.getPupeliuKiekis()==80);
//        if(kavosMasina.getVandensKiekis() == 1){
//            System.out.println("Testas praejo");
//        }else {
//            throw new Exception("Klaida");
//        }
    }
    @Test
    public void duotasUzpildytasKavosAparatas_patikrinaArUztenkaProduktu(){
        KavosMasina kavosMasina = new KavosMasina();
        kavosMasina.uzpildytiPupeles(100);
        kavosMasina.uzpildytiVandens(1);
        kavosMasina.tikrintiArUztenkaProduktu();

        Assertions.assertTrue(kavosMasina.tikrintiArUztenkaProduktu());
    }
    @Test
    public void duotasKavosAparatas_patikrinaArUztenkaProduktu_piresDarydamasKava(){
        KavosMasina kavosMasina = new KavosMasina();
        kavosMasina.uzpildytiPupeles(100);
        kavosMasina.uzpildytiVandens(1);

        Assertions.assertTrue(kavosMasina.darytiJuodaKava());
    }
}
