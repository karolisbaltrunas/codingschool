package main;

import java.util.Scanner;

public class Task4Menu {
    public static void main(String[] args) {
        int menu = giveMenu();
        System.out.println(doMath(menu));

    }
    static int giveMenu(){
        System.out.println("Pasirinkite meniu punkta");
        System.out.println("1.Skaiciu suma");
        System.out.println("2.Dvieju skaiciu sandauga");
        System.out.println("3.256 kvadratu");
        Scanner reader = new Scanner(System.in);
        int a = reader.nextInt();
        return a;
    }
    static String doMath(int choice){
        int answer=0;
        int a;
        int b;
        String text;
        Scanner reader = new Scanner(System.in);
        if(choice==1){
            System.out.println("Iveskite pirma skaiciu");
            a = reader.nextInt();
            System.out.println("Iveskite antra skaiciu");
            b = reader.nextInt();
            answer = a + b;
        }else if(choice==2){
            System.out.println("Iveskite pirma skaiciu");
            a = reader.nextInt();
            System.out.println("Iveskite antra skaiciu");
            b = reader.nextInt();
            answer = a * b;
        }else if(choice==3){
            answer= 256*256;
        }else{
            System.out.println("Ivesti netinkami parametrai");
        }
        text = "Atsakymas yra: " + answer;
        return text;
    }
}
