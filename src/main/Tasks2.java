package main;

public class Tasks2 {
    public static void main(String[] args) {
        boolean kintamasis = true;
        int kintamasis2 = 905;
        int kintamasis3 = 45;
        //1
        if (kintamasis == true) {
            System.out.println("Tiesa");
        } else {
            System.out.println("Netiesa");
        }
        //2
        if (kintamasis2 > 100) {
            System.out.println( kintamasis2 + " daugiau uz 100");
        }
        //3
        if (kintamasis3 > 100) {
            System.out.println( kintamasis3 + " daugiau uz 100");
        } else {
            System.out.println( kintamasis3 + " maziau uz 100");
        }
        //4
        if (kintamasis3 > 0 && kintamasis3 < 100) {
            System.out.println( kintamasis3 + " yra 0..100 rezyje");
        } else {
            System.out.println( kintamasis3 + " nera apibreztame rezyje");
        }
        //5
        System.out.println(checkIfNumberIsNegative(2));
        //6
        checkIfNumberIsBig(100);
        //7
        System.out.println(checkNumberInRange(5, 25, 75));
    }

    static boolean checkIfNumberIsNegative(int kintamasis) {
        if (kintamasis >= 0) {
            System.out.println("Skaicius " + kintamasis + " yra teigiamas");
            return false;
        } else {
            System.out.println("Skaicius " + kintamasis + " yra neigiamas");
            return true;
        }
    }

    static boolean checkIfNumberIsBig(int kintamasis) {
        if (kintamasis > 99) {
            System.out.println("Skaicius " + kintamasis + " yra trizenklis");
            return true;
        } else {
            System.out.println("Skaicius " + kintamasis + " yra dvizenklis");
            return false;
        }
    }

    public static boolean checkNumberInRange(int sk, int r1, int r2) {
        if (r1 > r2) {
            System.out.println("Netinkami reziai " + r1 + " yra daugiau uz " + r2);
        } else if (sk > r1 && sk < r2) {
            System.out.println("Skaicius " + sk + " yra tarp " + r1 + " ir " + r2);
            return true;
        } else {
            System.out.println("Skaicius " + sk + " nera tarp " + r1 + " ir " + r2);
        }
        return false;
    }
}
