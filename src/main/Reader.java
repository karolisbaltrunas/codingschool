package main;
import java.util.Scanner;

public class Reader {
    public static void main(String[] args) {
        System.out.println("iveskite 1 skaiciu");
        int skaicius = readAndPrint();
        System.out.println("iveskite 2 skaiciu");
        int skaicius2 = readAndPrint();
        System.out.println("Iveskite 1 skaiciu sumai");
        System.out.println("Iveskite 2 skaiciu skirtumui");
        System.out.println("Iveskite 3 skaiciu sandaugai");
        System.out.println("Iveskite 4 skaiciu dalybai");

        int veiksmas = readAndPrint();

        while(veiksmas<1||veiksmas>4){
            System.out.println("Pasirinktas netinkamas skaicius");
            veiksmas=readAndPrint();
        }

        if(veiksmas==1){
            System.out.println("Suma: " + (skaicius+skaicius2));
        }else if(veiksmas==2){
            System.out.println("Skirtumas " + (skaicius-skaicius2));
        }else if(veiksmas==3) {
            System.out.println("Sandauga " + (skaicius * skaicius2));
        }else if(veiksmas==4) {
            System.out.println("Dalyba " + (skaicius / skaicius2));
        }else{
            System.out.println("Pasirinktas netinkamas skaicius");
        }

    }

    public static int readAndPrint() {
        Scanner reader = new Scanner(System.in);
        int skaicius = reader.nextInt();

        return skaicius;
    }
}
