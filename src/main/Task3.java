package main;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        checkNumber();
        checkAge();
        tellInfo();
    }
    static void checkNumber(){
        System.out.println("Iveskite skaiciu");
        Scanner reader = new Scanner(System.in);
        int skaicius = reader.nextInt();
        if (skaicius>=0){
            System.out.println("Skaicius teigiamas");
        }else {
            System.out.println("Skaicius neigiamas");
        }
    }

    static void checkAge(){
        System.out.println("Iveskite savo amziu");
        Scanner reader = new Scanner(System.in);
        int skaicius = reader.nextInt();
        if(skaicius>100 || skaicius<0){
            System.out.println("Ivestas neteisingas amzius");
        }else if (skaicius>=18){
            System.out.println("Jus galite balsuoti");
        }else {
            System.out.println("Deja balsuoti negalite");
        }
    }

    static void tellInfo(){
        System.out.println("Iveskite skaiciu ne mazesni uz -50 ir ne didesni uz 1000");
        Scanner reader = new Scanner(System.in);
        int skaicius = reader.nextInt();

        while (skaicius<-50||skaicius>1000){
            System.out.println("Ivestas netinkamas skaicius");
            skaicius = reader.nextInt();
        }

        if(skaicius<0){
            System.out.println("Ivestas neigiamas skaicius");
        }else if (skaicius>100){
            System.out.println("Skaicius didesnis nei 100");
        }else if( skaicius>=40 && skaicius<=60){
            System.out.println("Skaicius yra tarp 40 ir 60");
        }else {
            System.out.println("Skaicius yra skaicius");
        }
    }
}
