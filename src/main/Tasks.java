package main;

public class Tasks {
    public static void main(String[] args) {
        int a = 6;
        int b = 3;
        System.out.println("Skicius + 1 = " + (++a));

        int c = a / b;
        System.out.println("Sveiku skaiciu dalyba " + c);

        System.out.println("Daugyba " + (a * b));

        String stringVar = "50";
        int stringSum = Integer.valueOf(stringVar) + a;
        System.out.println("String suma " + stringSum);

        double doubleVar = 5.5;
        System.out.println("Realiu skaiciu suma " + (doubleVar + 5.4));

        System.out.println(simpleReturn(5));

        printAndReturn(2);

        System.out.println(sumAndReturn(4, 2));

        System.out.println(returnString('a', 'v'));

        returnNone("2");

        System.out.println(returnTrue());

        daryk();

    }

    public static int simpleReturn(int a) {
        return a;
    }

    public static int printAndReturn(int a) {
        System.out.println(a);
        return a;
    }

    public static int sumAndReturn(int a, int b) {
        int c = a + b;
        return c;
    }

    public static String returnString(char c, char b) {
        String a;
        a = Character.toString(c) + Character.toString(b);
        return a;
    }

    public static void returnNone(String a) {
        int b = 5;
        int c = b + Integer.valueOf(a);
        System.out.println(c);
    }

    public static boolean returnTrue() {
        return true;
    }

    static void daryk() {

        return;
    }
}
