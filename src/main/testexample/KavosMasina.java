package main.testexample;

public class KavosMasina {
    private int vandensKiekis;
    private int pupeliuKiekis;

    public void uzpildytiPupeles(int pupeliuKiekis) {
        this.pupeliuKiekis+=pupeliuKiekis;
    }

    public void uzpildytiVandens(int vandensKiekis) {
        this.vandensKiekis+=vandensKiekis;
    }
    public boolean darytiJuodaKava(){
        if (vandensKiekis>=1&&pupeliuKiekis>=20){
            vandensKiekis--;
            pupeliuKiekis-=20;
            return true;
        }
        System.out.println("Neuztenka produktu");
        return false;
    }

    public int getVandensKiekis() {
        return vandensKiekis;
    }

    public void setVandensKiekis(int vandensKiekis) {
        this.vandensKiekis = vandensKiekis;
    }

    public int getPupeliuKiekis() {
        return pupeliuKiekis;
    }

    public void setPupeliuKiekis(int pupeliuKiekis) {
        this.pupeliuKiekis = pupeliuKiekis;
    }

    public  boolean tikrintiArUztenkaProduktu() {
        boolean arUzUztenka = pupeliuKiekis>20&&vandensKiekis>0;
        return arUzUztenka;
    }

}
