package main;

import java.util.Scanner;

public class Student {
    double pazymiuVidurkis;
    String ak;

    public static void main(String[] args) {
        Student go = new Student();
        go.nustatytiKoda("44484445");
        go.nustatytiPazymiuVidurki(5, 6, 5);
        go.spausdinti();

        Student go2 = new Student();
        go2.nustatytiKoda("111111");
        go2.nustatytiPazymiuVidurki(3, 2, 1);
        go2.spausdinti();
    }


    public void nustatytiPazymiuVidurki(double pazymys1, double pazymys2, double pazymys3) {
        pazymiuVidurkis = (pazymys1 + pazymys2 + pazymys3) / 3;
    }
//    double size = 1;
//    double pazymiuSuma=7;
//    public double nustatytiPazymiuVidurki(double pazymys1){
//        double pazymiuSuma =+ pazymys1;
//        size =+ 1;
//        return pazymiuSuma;
//}

    public void nustatytiKoda(String ak) {
        this.ak = ak;
    }

    //
    public void spausdinti() {
        if (pazymiuVidurkis > 5 && pazymiuVidurkis < 8) {
            System.out.println(pazymiuVidurkis);
//             System.out.println(pazymiuSuma/size);
        } else {
            System.out.println("nerodysiu");
        }
        System.out.println(ak);
    }

    ///Pasiekiamumas
    private void privatus() {
        System.out.println("Privatus");
    }

    void modifier() {
        System.out.println("Be pasiekiamumo");
    }

    protected void protectedMethod() {
        System.out.println("Apsaugotas");
    }

    public void publicMethod() {
        System.out.println("Public");
    }

    //static leidzia atlikti veiksmus nesusikurus objekto
    public static int sumuoti(int a, int b) {
        int suma;
        suma = a + b;
        Student naujas = new Student();
        naujas.privatus();
        return suma;
    }
}


