package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Task6array {
    public static void main(String[] args) {

        int array[] = {1, 2, 3, 4, 5};

//        for (int i=0; i<array.length; i++){
//            System.out.println(array[i]);
//        }

//        Masyvo suma
//        System.out.println(sumArray(array));

//        Masyvo perrasimas
//        System.out.println("Senasis masyvas: " +  Arrays.toString(array));
//        int newArray[]= rewriteArray(array);
//        System.out.println("Naujas masyvas " + Arrays.toString(newArray));

//        Masyvo ivedimas
//        int yourArray[] = getArray();
//        System.out.println("Jusu masyvas: " + Arrays.toString(yourArray));

//        Masyvo ivedimas ir veiksmai su juo
//        getArrayTurbo();

//        Masyvu lyginimas
//        int firstArray[]={1, 2, 3, 4, 6};
//        int secondArray[]={1, 2, 5, 6, 6};
//        compareArrays(firstArray, secondArray);
//        readNumbers();
//        int array3[] = {5,4,2,3,5,4,2,5,0,9,6,8,7,};
//        System.out.println(findNumber(array3, 9));
//        System.out.println(arrayToString(array3));
        ciklai();
    }

    static int sumArray(int array[]) {
        int result = 0;
        for (int i = 0; i < array.length; i++) {
            result = result + array[i];
        }
        return result;
    }

    static int[] rewriteArray(int array[]) {
        int newArray[] = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i] * 2;
        }
        return newArray;
    }

    static int[] getArray() {
        Scanner reader = new Scanner(System.in);
        int array[] = new int[5];
        for (int i = 0; i < 5; i++) {
            System.out.println("Iveskite " + (i + 1) + "-aji skaiciu");
            int number = reader.nextInt();
            array[i] = number;
        }
        return array;
    }

    static void getArrayTurbo() {
        Scanner reader = new Scanner(System.in);
        System.out.println("Iveskite masyvo dydi");

        int size = reader.nextInt();
        int array[] = new int[size];

        int sum = 0, avg = 0, mult = 1;
        for (int i = 0; i < size; i++) {
            System.out.println("Iveskite " + (i + 1) + "-aji skaiciu");
            int number = reader.nextInt();
            array[i] = number;
            sum = sum + number;
            avg = sum / size;
            mult = mult * number;
        }
        System.out.println("Ivestas masyvas: " + Arrays.toString(array));
        System.out.println("Masyvo skaiciu suma: " + sum);
        System.out.println("Masyvo skaiciu vidurkis: " + avg);
        System.out.println("Masyvo skaiciu sandauga: " + mult);
    }

    static void compareArrays(int first[], int second[]){
        int firstLength = first.length;
        int secondLengt = second.length;
        int maxLength = 0;
        System.out.println("Pirmaji masyva sudaro: " + firstLength);
        System.out.println("Antraji masyva sudaro: " + secondLengt);

        if (firstLength>secondLengt){
            System.out.println("Pirmasis masyvas yra ilgesnis");
            maxLength = firstLength;
        }else if(secondLengt>firstLength){
            System.out.println("Antrasis masyvas yra ilgesnis");
            maxLength = secondLengt;
        }else {
            System.out.println("Masyvai yra lygus");
            maxLength = firstLength;
            for(int i=0; i<maxLength; i++ ){
                if(first[i]==second[i]){
                    System.out.println(i + "-ieji masyvo nariai sutampa, ju reiksmes =" + first[i]);
                }
            }
        }
    }

    static void readNumbers(){
        Scanner reader = new Scanner(System.in);
        int array [] = new int[11];
        while (true){
            System.out.println("Iveskite skaiciu nuo 1 iki 10 arba -1, kad baigti");
            int input = reader.nextInt();
            if(input==-1) {
                System.out.println("Buvo ivesti skaiciai: " + Arrays.toString(array));
                break;
            }else if(input>=0&&input<=10){
                array[input]++;
            }else {
                System.out.println("Ivesta netinkama reiksme");
            }
        }
    }

    static int findNumber (int array[], int param){
        int number=0;
        for (int i=0; i<array.length; i++){
            if (array[i]==param){
                number=(i+1);
                break;
            }else {
                number=-1;
            }
        }
        return number;
    }

    static String arrayToString(int array[]){
        String col = "";
        for (int i=0; i<array.length; i++){
            col+=array[i] + ",";
        }
        return col;
    }

    static void ciklai(){
        int [][] matrix = new int [10] [10];
        for(int i=0; i<10; i++){
            for(int j=0; j<10; j++){
                matrix[i][j]=i+j+1;
            }
        }

        int [][] matrix2 = new int[10][10];
        int i = 0;
        while(i<10){
            int j=0;
            while(j<10){
                matrix2 [i][j]= 10 -((i+j)*2);
                j++;
            }
            i++;
        }
        System.out.println(Arrays.deepToString(matrix2));
    }
}
