package lt.vsc.karolis;

import java.util.Scanner;

public class TimeCounting {
    //Skaiciuoti minutes
    static void countMinutes(int var){
        int days=0,hours=0,minutes=0;

        days=var/1440;
        System.out.println(days + " dienos");
        hours=(var%1440)/60;
        System.out.println(hours + " valandos");
        minutes=(var%1440)%60;
        System.out.println(minutes + " minutes");
    }
    static void convertMinutes(){
        Scanner reader = new Scanner(System.in);
        System.out.println("Iveskite dienu skaiciu: ");
        int days = reader.nextInt();
        System.out.println("Iveskite valandu skaiciu: ");
        int hours = reader.nextInt();
        System.out.println("Iveskite minuciu skaiciu: ");
        int minutes = reader.nextInt();

        int converted = (days*1440)+(hours*60) + minutes;
        System.out.println("Ivestame laike yra: " + converted + " ");
    }
}
