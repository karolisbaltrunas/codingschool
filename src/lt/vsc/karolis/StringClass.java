package lt.vsc.karolis;

import java.util.Arrays;
import java.util.Scanner;

public class StringClass {
    static void countStrings(){
//        String string = "abc";
//        System.out.println(string);
//
//        string=string.concat("ef");
//        System.out.println(string);
//
//        StringBuilder str = new StringBuilder("abc");
//        System.out.println(str.toString());
//        str.append("ef");
//        System.out.println(str.toString());

        System.out.println("Iveskite teksta");
        Scanner reader = new Scanner(System.in);
        String myString = reader.next();
        System.out.println("Ilgis: " + myString.length());

        System.out.println("Pirmasis a indeksas " + myString.indexOf("a"));
        System.out.println("Paskutinis a indeksas " + myString.lastIndexOf("a"));
        System.out.println("Be trapu: " + myString.replaceAll("\\s",""));
        System.out.println("Didziosiomis: " + myString.toUpperCase());
        System.out.println("Prasideda su abc: " + myString.startsWith("abc"));
        System.out.println("Pasibaigia su abc: " + myString.endsWith("abc"));
        System.out.println("Talpina zodi test savo viduje: " + myString.contains("test"));
        System.out.println("abc pakeisti i def: " + myString.replace("abc", "def"));
        System.out.println("Surasti abd pradzios indeksa: " + myString.indexOf("abd"));
        System.out.println("Iskirpti teksta iki abc: " + myString.substring(myString.indexOf("abc")) );
        System.out.println("Iskirpti teksta iki abc: " + myString.substring(0, myString.indexOf("abc")));
        System.out.println("Iskirpti teksta iki abc: " + myString.substring(0, myString.lastIndexOf("abc")));


    }

    public static void main(String[] args) {
        String textWithWhiteSpaces = "laba diena as esu Karolis";
        String [] textArray = textWithWhiteSpaces.split("\\s+");
        System.out.println(Arrays.toString(textArray));
    }
}
