package lt.vsc.karolis;

import java.util.Scanner;

public class Countings {
    //Lyginis ar ne
    static void evenOrOdd(int var) {
        if (var % 2 == 0) {
            System.out.println("Skaicius lyginis");
        } else {
            System.out.println("Skaicius nelyginis");
        }
    }

    //Rasti skaiciaus paskutini skaitmeni
    static void findLastNumber(int var) {
        System.out.println("paskutinis skaicius: " + var % 10);
    }

    //Rasti pirmaji skaitmeni
    static void findFirsNumber(int var) {
        while (var < -9 || var > 9) {
            var /= 10;
        }
        System.out.println("Pirmasis skaicius: " + Math.abs(var));
    }

    //Keliazianklis skaicius
    static void findNumberLength(double var) {
        var = Math.abs(var);
        int length = (int) (Math.log10(var) + 1);
        System.out.println("Skaiciaus ilgis: " + length);
    }

    //Skaiciai kurie dalijasi is daliklio
    static void showDivider(int devider, int number) {
        int sum = 0;
        System.out.println("Skaiciai kurie dalijasi is: " + devider);
        for (int i = 0; i < number; i++) {
            sum += devider;
            System.out.print(sum + ",");
        }
    }

    //Atspausdinti atbulai
    static void printBackwards(int var) {
        System.out.println("");
        System.out.println("Skaiciai atvirkscia tvarka");
        int inverted = 0;
        while (var != 0) {
            inverted = 10 * inverted + var % 10;
            //System.out.print(var%10);
            var /= 10;
        }

        System.out.println(inverted);
    }

    //Metodas kuris spausdina kvadrata
    static void makeSquare(int a, int b) {
        int[][] matrix = new int[a][b];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                System.out.print("#");
            }
            System.out.println("");
        }
    }

    //Metodas kuris spausdina kazka
    static void makeSomething(int a, int b) {
        int[][] matrix = new int[a][b];
        for (int i = 0; i < a; i++) {
            if (i % 2 != 0) {
                System.out.print(" ");
                for (int j = 0; j < b - 2; j++) {
                    System.out.print("#");
                }
            } else {
                for (int j = 0; j < b; j++) {
                    System.out.print("#");
                }
            }
            System.out.println("");
        }
    }

    //Keliazinkli antras budas
    static void findNumberLength2(int var) {
        var = Math.abs(var);
        int count = 1;
        while (var > 9) {
            var = var / 10;
            count++;
        }
        System.out.println("zenklai: " + count);
    }

    //Daugybos lentele
    static void makeMultTable() {
        int table[][] = new int[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                int var = table[i][j] = (i + 1) * (j + 1);
                if (var < 10) {
                    System.out.print("|" + var + " |");
                } else {
                    System.out.print("|" + var + "|");
                }
            }
            System.out.println(" ");
        }
    }

    //Namu darbai, faktorialas
    static void countFa(int var) {
        int answ = var;
        while (var > 1) {
            answ *= var - 1;
            var -= 1;
        }
        System.out.println("Faktorialas yra: " + answ);
    }

    //Fibonacio seka
    static void giveFibonacciSeq() {
        Scanner reader = new Scanner(System.in);
        System.out.println("Kiek sekos skaiciu norite pamatyti?");
        int num = reader.nextInt();
        int seq2[] = new int[num];
        seq2[0] = 0;
        seq2[1] = 1;
        for (int i = 0; i < num - 2; i++) {
            seq2[i + 2] = seq2[i] + seq2[i + 1];
        }
        for (int i = 0; i < num; i++) {
            System.out.print(seq2[i] + ",");
        }
        System.out.println("\nKelinta sekos skaiciu norite suzinoti");
        int place = reader.nextInt();
        System.out.println(seq2[place]);
    }

    //Eglute
    public static void printChristmasTree(int height) {
        int offset = height;
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < offset; j++) {
                System.out.print(" ");
            }
            for (int k = 0; k < 2 * i - 1; k++) {
                System.out.print("*");
            }
            System.out.println();
            offset--;
        }
    }
}
