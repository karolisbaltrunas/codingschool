package lt.vsc.karolis.coffeeMachine;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import lt.vsc.karolis.coffeeMachine.cupHierarchy.*;

public class CoffeMashine implements CoffeeMashineInterface{
    private static final int USAGE_CONST=10;
    public int usageCount=0;
    public Products products;
    public static int counter=0;
    static Map<String, Cup> cupMap = new HashMap<String, Cup>();
    private boolean alreadyExecuted=false;


    public CoffeMashine(int sugar, int coffeBeans, int water){
        this.products = new Products();
        this.products.sugar=sugar;
        this.products.coffeBeans=coffeBeans;
        this.products.water=water;
        counter++;
    }
    public CoffeMashine(Products products1){
        this.products = products1;
        counter++;
    }
    public CoffeMashine(){
        counter++;
    }

    public int openMenu(){
        Scanner reader = new Scanner(System.in);
        int choice=0;
        while(choice==0){
            if(products.sugar<120){
                System.out.println("Papildykite cukraus");
                products.fillSugar();
            }else if(products.coffeBeans<160){
                System.out.println("Papildykite kavos pupeles");
                products.fillCoffeBeans();
            }else if(products.water<650){
                System.out.println("Papildykite vandens");
                products.fillWater();
            }else if(usageCount>=USAGE_CONST){
                System.out.println("Praplaukite kavos aparata!");
                return 5;
            }else {
                System.out.println("Aparatas pasiruoses");
                System.out.println("___MENIU___");
                System.out.println("Pasirinkite norima kava: \n1.Juoda kava \n2.Latte \n3.Expresso \n4.Capuccino \n5.Servisas \n6.Kad baigti kavos gaminima.");
                choice = reader.nextInt();
            }
        }
        return choice;
    }
    public int openServiceMenu(){
        Scanner reader = new Scanner(System.in);
        System.out.println("___Serviso Meniu___");
        System.out.println("1.Info apie aparata \n2.Atlikti plovima \n3.Papildyti produktu \n4.Isvalyti produktus \n5.Iseiti is serviso meniu");
        int choice = reader.nextInt();
        return choice;
    }
    public void giveInfo(){
        System.out.println("Siuo metu aparate yra:");
        System.out.println(products.sugar + "g. cukraus.");
        System.out.println(products.coffeBeans + "g. kavos pupeliu.");
        System.out.println(products.water + "g. vandens.");
        System.out.println("Iki plovimo liko: " + (USAGE_CONST-usageCount));
    }
    public Cup makeCoffe(int choice, Cup cup){
        CoffeeRecepies recepie = CoffeeRecepies.pickRecepieByNum(choice);
        products.sugar-=recepie.sugar;
        products.coffeBeans-=recepie.coffeeBeans*cup.mult;
        products.water-=recepie.water;
        cup.isCoffeeMade=true;
        System.out.println(recepie.coffeeName + " pagaminta. " + cup.size);
        return cup;
    }
    public int fillProducts(){
        Scanner reader = new Scanner(System.in);
        System.out.println("1.Papildyti cukraus \n2.Papildyti kavos pupeliu \n3.Papildyti vandens \n4.Iseiti is serviso meniu");
        return reader.nextInt();
    }

    ///////////////Paskutines uzduotys, puodeliai
    public static void fillCupMap(){
        BigCoffeeCup bigCoffeeCup =new BigCoffeeCup();
        MediumCoffeeCup mediumCoffeeCup =new MediumCoffeeCup();
        SmallCoffeeCup smallCoffeeCup =new SmallCoffeeCup();
        cupMap.put("Didelis", bigCoffeeCup);
        cupMap.put("Vidutinis", mediumCoffeeCup);
        cupMap.put("Mazas", smallCoffeeCup);
    }
    public void addNewCupType (){
        if(!alreadyExecuted) {
            fillCupMap();
            alreadyExecuted = true;
        }
        Scanner reader = new Scanner(System.in);
        System.out.println("Pridedama nauja puodelio rusis.");
        Cup newCup = new Cup();
        newCup.isCoffeeMade=false;
        System.out.println("Iveskite rusies pavadinima");
        newCup.size=reader.nextLine();
        System.out.println("Iveskite produktu kiekio daugikli");
        newCup.mult=reader.nextInt();
        cupMap.put(newCup.size, newCup);
        System.out.println("Naujas puodelio tipas buvo pridetas.");
    }
    public void deleteCupType(){
        if(!alreadyExecuted) {
            fillCupMap();
            alreadyExecuted = true;
        }
        Scanner reader = new Scanner(System.in);
        System.out.println("Iveskite norimo pasalinti tipa:");
        String type = new String();
        type = reader.nextLine();
        cupMap.remove(type);

//        try {
//
//        }
    }
}
