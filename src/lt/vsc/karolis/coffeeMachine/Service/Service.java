package lt.vsc.karolis.coffeeMachine.Service;

import lt.vsc.karolis.coffeeMachine.CoffeMashine;
import lt.vsc.karolis.coffeeMachine.Products;

public class Service implements ServiceInterface{

    public CoffeMashine createNewMashine(Products products){
        return new CoffeMashine(products);
    }
    public void cleanMyMashine (CoffeMashine myMashine){
        myMashine.usageCount=0;
        System.out.println("Kavos aparatas isplautas.");
    }
    public CoffeMashine[] createMashines(int number){
        CoffeMashine mashinesArray [] = new CoffeMashine[number];
        for(int i=0; i<number; i++){
            mashinesArray[i]= new CoffeMashine();
        }
        return mashinesArray;
    }
    public int cleanProducts (CoffeMashine myMashine){
        int sum = myMashine.products.sugar +myMashine.products.coffeBeans+myMashine.products.water;
        myMashine.products.sugar=0;
        myMashine.products.coffeBeans=0;
        myMashine.products.water=0;
        return sum;
    }
    public int[] cleanProducts (CoffeMashine[] mashinesArray){
        int sum [] = new int[mashinesArray.length];
        for(int i=0; i<mashinesArray.length; i++){
            sum [i] = mashinesArray[i].products.sugar +mashinesArray[i].products.coffeBeans+mashinesArray[i].products.water;
            mashinesArray[i].products.sugar=0;
            mashinesArray[i].products.coffeBeans=0;
            mashinesArray[i].products.water=0;
        }
        return sum;
    }

    public void fillMashines(CoffeMashine[] mashinesArray){
        Products products2 = new Products();
        products2.sugar=1234;
        products2.water=1234;
        products2.coffeBeans=1234;
        for (int i=0; i<mashinesArray.length; i++){
            mashinesArray[i].products=products2 ;
        }
    }
    public void fillMashines2(CoffeMashine[] mashinesArray){
        Products products2 []= new Products[mashinesArray.length];
           int qua =0;
        for (int i=0; i<mashinesArray.length; i++){
            qua+=500;
            products2[i]= new Products();
            products2[i].sugar=qua;
            products2[i].water=qua;
            products2[i].coffeBeans=qua;
            mashinesArray[i].products=products2[i] ;
        }
    }
}
