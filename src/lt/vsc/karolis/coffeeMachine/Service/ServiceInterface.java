package lt.vsc.karolis.coffeeMachine.Service;

import lt.vsc.karolis.coffeeMachine.CoffeMashine;
import lt.vsc.karolis.coffeeMachine.Products;

public interface ServiceInterface {
    /**
     * Sukuria nauja kavos aparata.
     * @param products - paduodami produktai
     * @return - grazinamas naujas kavos aparatas
     */
    public CoffeMashine createNewMashine(Products products);

    /**
     * Nunulinamas panaudojimu skaicius.
     * @param myMashine -paduodamas kavos aparatas.
     */
    public void cleanMyMashine (CoffeMashine myMashine);

    /**
     * Isvalomi visi produktai is paduodamo aparato.
     * @param myMashine - paduodamas kavos aparatas.
     * @return - grazinma isvalytu produktu suma.
     */
    public int cleanProducts (CoffeMashine myMashine);
}
