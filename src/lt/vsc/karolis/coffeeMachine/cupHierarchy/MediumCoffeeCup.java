package lt.vsc.karolis.coffeeMachine.cupHierarchy;

public class MediumCoffeeCup extends Cup{

    public MediumCoffeeCup() {
        this.size = "Vidutinis puodelis";
        this.isCoffeeMade = false;
        this.mult = 1.5;
    }

    public void cupInfo(){
        System.out.println("Puodelio dydis yra: " + size);
        System.out.println("Produktu daugiklis: " + mult);
    }
}
