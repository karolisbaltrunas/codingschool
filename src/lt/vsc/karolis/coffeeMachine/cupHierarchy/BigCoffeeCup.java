package lt.vsc.karolis.coffeeMachine.cupHierarchy;

public class BigCoffeeCup extends Cup{

    public BigCoffeeCup() {
        this.size = "Didelis puodelis";
        this.isCoffeeMade = false;
        this.mult = 2;
    }

    public void cupInfo(){
        System.out.println("Puodelio dydis yra: " + size);
        System.out.println("Produktu daugiklis: " + mult);
    }
}
