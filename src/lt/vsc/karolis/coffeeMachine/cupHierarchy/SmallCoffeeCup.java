package lt.vsc.karolis.coffeeMachine.cupHierarchy;

public class SmallCoffeeCup extends Cup{

    public SmallCoffeeCup() {
        this.size = "Mazas puodelis";
        this.isCoffeeMade = false;
        this.mult = 1;
    }

    public void cupInfo(){
        System.out.println("Puodelio dydis yra: " + size);
        System.out.println("Produktu daugiklis: " + mult);
    }
}
