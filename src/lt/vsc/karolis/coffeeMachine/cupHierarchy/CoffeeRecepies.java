package lt.vsc.karolis.coffeeMachine.cupHierarchy;

public enum CoffeeRecepies {
    JUODA (1, "Juoda kava", 40, 50, 300),
    LATTE (2, "Latte", 60, 40, 350),
    EXPRESSO(3, "Expresso", 20, 80, 150),
    CAPUCCINO(4, "Capuccino", 60, 60, 350);

    int number;
    public String coffeeName;
    public int sugar;
    public int coffeeBeans;
    public int water;

    CoffeeRecepies(int number, String coffeeName, int sugar, int coffeeBeans, int water) {
        this.number = number;
        this.coffeeName = coffeeName;
        this.sugar = sugar;
        this.coffeeBeans = coffeeBeans;
        this.water = water;
    }

    public static CoffeeRecepies pickRecepieByNum(int number){
        for (CoffeeRecepies recepie : CoffeeRecepies.values()) {
                if (recepie.number == number){
                    return recepie;
                }
        }
        return JUODA;
    }
}
