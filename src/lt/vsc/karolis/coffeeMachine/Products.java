package lt.vsc.karolis.coffeeMachine;

import java.util.Scanner;

public class Products {
    public int sugar;
    public int coffeBeans;
    public int water;

    public void fillSugar(){
        Scanner reader = new Scanner(System.in);
        System.out.println("Siuo metu aparate yra: " + this.sugar + "g. cukraus");
        System.out.println("Iveskite kiek cukraus norite papildyti gramais (max 2000g.).");
        int sugarQua = reader.nextInt();
        if(sugarQua>=2000){
            System.out.println("Ivestas didesnis nei maksimalus kiekis, todel cukraus kiekis uzpildytas maksimaliai.");
            this.sugar+=2000;
        }else if(sugarQua<2000&&sugarQua>0){
            System.out.println("Aparatas papildytas " + sugarQua + " g. cukraus.");
            this.sugar += sugarQua;
        }else{
            System.out.println("Ivestas netinkamas skaicius");
        }
    }
    public void fillCoffeBeans(){
        Scanner reader = new Scanner(System.in);
        System.out.println("Siuo metu aparate yra: " + this.coffeBeans + "g. kavos pupeliu");
        System.out.println("Iveskite kiek kavos norite papildyti gramais (max 5000g.).");
        int beansQua = reader.nextInt();
        if(beansQua>=5000){
            System.out.println("Ivestas didesnis nei maksimalus kiekis, todel pupeliu kiekis uzpildytas maksimaliai.");
            this.coffeBeans+=5000;
        }else if(beansQua<5000&&beansQua>0){
            System.out.println("Aparatas papildytas " + beansQua + " g. pupeliu.");
            this.coffeBeans += beansQua;
        }else{
            System.out.println("Ivestas netinkamas skaicius");
        }
    }
    public void fillWater(){
        Scanner reader = new Scanner(System.in);
        System.out.println("Siuo metu aparate yra: " + this.water + "g. vandens");
        System.out.println("Iveskite kiek vandens norite papildyti gramais (max 10000g.).");
        int waterQua = reader.nextInt();
        if(waterQua>=10000){
            System.out.println("Ivestas didesnis nei maksimalus kiekis, todel vandens kiekis uzpildytas maksimaliai.");
            this.water+=10000;
        }else if(waterQua<10000&&waterQua>0){
            System.out.println("Aparatas papildytas " + waterQua + " g. vandens.");
            this.water += waterQua;
        }else{
            System.out.println("Ivestas netinkamas skaicius");
        }
    }

    public Products productsCopy(){
        Products newProductsCopy = new Products();
        newProductsCopy.sugar = this.sugar;
        newProductsCopy.coffeBeans = this.coffeBeans;
        newProductsCopy.water = this.water;
        return newProductsCopy;
    }
}
