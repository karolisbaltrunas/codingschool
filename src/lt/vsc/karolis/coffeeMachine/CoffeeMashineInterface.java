package lt.vsc.karolis.coffeeMachine;

import lt.vsc.karolis.coffeeMachine.cupHierarchy.Cup;

public interface CoffeeMashineInterface {
    /**
     * Atvaizduoja pagrindini meniu su kavos pasirinkimais, galimybe patekti i serviso meniu ir galimybe uzdaryti programa
     * @return - grazina pasirinkima (ivesta reiksme)
     */
    public int openMenu();

    /**
     * Atvaizduoja service menu, su galimybe grizti i pagrindini meniu.
     * @return - grazina pasirinkima
     */
    public int openServiceMenu();

    /**
     * Isveda informacija apie kavos aprata, produktus ir t.t.
     */
    public void giveInfo();

    /**
     * Gamina kava, pagal receptus minusuoja produktus is kavos aparato.
     * @param choice - pasirinkimas is meniu, kuris receptas turi buti gaminamas.
     * @param cup - sukurtas puodelis, kuris pagal talpa keicia sunaudojamu produktu kiekius.
     * @return - grazina puodeli su pakeista reiksme, kad puodelis yra pripildytas.
     */
    public Cup makeCoffe(int choice, Cup cup);

    /**
     * Atvaizduoja pasirinkimus ka galima papildyti ir nuskaito pasirinkima.
     * @return - grazina pasirinkima.
     */
    public int fillProducts();
}
