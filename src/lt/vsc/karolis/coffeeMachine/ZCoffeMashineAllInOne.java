package lt.vsc.karolis.coffeeMachine;

import java.util.Scanner;

public class ZCoffeMashineAllInOne {
   private int sugar;
   private int coffeBeans;
   private int water;
   private int usageCount;
   private int USAGE_CONST=10;

    public void fillSugar(){
        Scanner reader = new Scanner(System.in);
        System.out.println("Iveskite kiek cukraus norite papildyti gramais (max 2000g.).");
        int sugarQua = reader.nextInt();
        if(sugarQua>=2000){
            System.out.println("Ivestas didesnis nei maksimalus kiekis, todel cukraus kiekis uzpildytas maksimaliai.");
            this.sugar+=2000;
        }else if(sugarQua<2000&&sugarQua>0){
            System.out.println("Aparatas papildytas " + sugarQua + " g. cukraus.");
            this.sugar += sugarQua;
        }else{
            System.out.println("Ivestas netinkamas skaicius");
        }
    }
    public void fillCoffeBeans(){
        Scanner reader = new Scanner(System.in);
        System.out.println("Iveskite kiek kavos norite papildyti gramais (max 5000g.).");
        int beansQua = reader.nextInt();
        if(beansQua>=5000){
            System.out.println("Ivestas didesnis nei maksimalus kiekis, todel pupeliu kiekis uzpildytas maksimaliai.");
            this.coffeBeans+=5000;
        }else if(beansQua<5000&&beansQua>0){
            System.out.println("Aparatas papildytas " + beansQua + " g. pupeliu.");
            this.coffeBeans += beansQua;
        }else{
            System.out.println("Ivestas netinkamas skaicius");
        }
    }
    public void fillWater(){
        Scanner reader = new Scanner(System.in);
        System.out.println("Iveskite kiek vandens norite papildyti gramais (max 10000g.).");
        int waterQua = reader.nextInt();
        if(waterQua>=10000){
            System.out.println("Ivestas didesnis nei maksimalus kiekis, todel vandens kiekis uzpildytas maksimaliai.");
            this.water+=10000;
        }else if(waterQua<10000&&waterQua>0){
            System.out.println("Aparatas papildytas " + waterQua + " g. vandens.");
            this.water += waterQua;
        }else{
            System.out.println("Ivestas netinkamas skaicius");
        }
    }
    public void makeCoffe(){
        Scanner reader = new Scanner(System.in);
        boolean circle=true;
        while(circle){
            outerloop:
            if(sugar<60){
                System.out.println("Papildykite cukraus kieki");
                fillSugar();
            }else if(coffeBeans<80){
                System.out.println("Papildykite kavos pupeles");
                fillCoffeBeans();
            }else if(water<350){
                System.out.println("Papildykite vandens");
                fillWater();
            }else if(usageCount>=USAGE_CONST){
                System.out.println("Praplaukite kavos aparata, iveskite 1");
                if(reader.nextInt()==1){
                    usageCount=0;
                    System.out.println("Kavos aparatas isplautas");
                    break outerloop;
                }else{
                    System.out.println("Aparatas neisplautas");
                    break outerloop;
                }
            }else{
                System.out.println("Aparatas pasiruoses \nProduktu netruksta \nAparatas svarus");
                System.out.println("Aparate siuo metu yra:");
                System.out.println("Cukraus: " + sugar);
                System.out.println("Pupeliu: " + coffeBeans);
                System.out.println("Vandens: " + water);
                System.out.println("Iki plovimo galite padaryti " + USAGE_CONST + " kavos puodeliu.");
                System.out.println("Pasirinkite norima kava: \n1.Juoda kava \n2.Latte \n3.Expresso \n4.Kad baigti kavos gaminima.");
                int choice = reader.nextInt();
                if(choice==1){
                    sugar=sugar-40;
                    coffeBeans = coffeBeans-50;
                    water = water - 300;
                    System.out.println("Juoda kava paruosta");
                    usageCount++;
                    break outerloop;
                }else if(choice==2){
                    sugar=sugar-60;
                    coffeBeans = coffeBeans-40;
                    water = water - 350;
                    System.out.println("Latte paruosta");
                    usageCount++;
                    break outerloop;
                }else if(choice==3){
                    sugar=sugar-20;
                    coffeBeans = coffeBeans-80;
                    water = water - 150;
                    System.out.println("Expresso paruosta");
                    usageCount++;
                    break outerloop;
                }else if(choice==4){
                    circle = false;
                }else{
                    System.out.println("Ivestas netinkamas parametras");
                }
            }
        }
    }
    public ZCoffeMashineAllInOne(int sugar, int coffeBeans, int water ){
        this.sugar=sugar;
        this.coffeBeans=coffeBeans;
        this.water=water;
    }
    public ZCoffeMashineAllInOne(int sugar, int coffeBeans ){
        this.sugar=sugar;
        this.coffeBeans=coffeBeans;
    }

    public static void main(String[] args) {
        ZCoffeMashineAllInOne firstCoffeMashine = new ZCoffeMashineAllInOne(200,5000, 5000);
        ZCoffeMashineAllInOne secondCoffeMashine = new ZCoffeMashineAllInOne(200,500);
        secondCoffeMashine.makeCoffe();
        //firstCoffeMashine.makeCoffe();

    }
}
