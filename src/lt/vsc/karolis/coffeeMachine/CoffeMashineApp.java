package lt.vsc.karolis.coffeeMachine;
import lt.vsc.karolis.coffeeMachine.Service.Service;
import lt.vsc.karolis.coffeeMachine.cupHierarchy.BigCoffeeCup;
import lt.vsc.karolis.coffeeMachine.cupHierarchy.Cup;

import java.util.InputMismatchException;


public class CoffeMashineApp {
    public static void main(String[] args) {
        Service myMashineService = new Service();
        CoffeMashine coffeMashine = new CoffeMashine(50, 5000, 10000);

//        Products myProducts = new Products();
//        Products productsCopy = myProducts.productsCopy();
//        CoffeMashine coffeMashine2 = myMashineService.createNewMashine(productsCopy);
//
//        coffeMashine.deleteCupType();
        while (true){
            try{
                int choice = coffeMashine.openMenu();
                int serviceChioce=0;
                int fillChoice=0;
                BigCoffeeCup bigCup = new BigCoffeeCup();
                if(choice==5){
                    serviceChioce = coffeMashine.openServiceMenu();
                }else if(choice==6){
                    System.out.println("Buvo sukurta " + coffeMashine.counter + " kavos aparatai");
                    System.exit(0);
                }else{
                    Cup returnedCup = coffeMashine.makeCoffe(choice, bigCup);
                    if (returnedCup.isCoffeeMade){
                        System.out.println("Viskas ivyko sekmingai !");
                    }
                }
                if(serviceChioce==1){
                    coffeMashine.giveInfo();
                }else if (serviceChioce==2){
                    myMashineService.cleanMyMashine(coffeMashine);
                }else if(serviceChioce==3){
                    fillChoice = coffeMashine.fillProducts();
                }else if(serviceChioce==4){
                    int sum =  myMashineService.cleanProducts(coffeMashine);
                    System.out.println("Produktai isvalyti.\nBendra produktu suma " + sum);
                }
                if (fillChoice==1){
                    coffeMashine.products.fillSugar();
                }else if(fillChoice==2){
                    coffeMashine.products.fillCoffeBeans();
                }else if (fillChoice==3){
                    coffeMashine.products.fillWater();
                }
            }catch (InputMismatchException e){
                System.out.println("Ivestas blogas parametras");
            }
        }
    }
}
