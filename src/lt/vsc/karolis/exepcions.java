package lt.vsc.karolis;

import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class exepcions {
    public static void main(String[] args) {
        int a=0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Iveskite skaiciu");
        boolean sucess=false;
        do{
            try {
                a = scanner.nextInt();
                sucess=true;
            }catch (InputMismatchException e){
                scanner.next();
                System.out.println("Ivedete bloga. Pakartokite");
                sucess=false;
            }
        }while (!sucess);

        System.out.println("ivestas skaicius kvadratas " + a*a);
    }
}
