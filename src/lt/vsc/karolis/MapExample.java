package lt.vsc.karolis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MapExample {

    public static void main(String[] args) {
//        Map <String, String> myMap = new HashMap<String, String>();
//        myMap.put("Pirmas", "a");
//        myMap.put("Antras", "b");
//        myMap.put("Trecias", "a");
//

        List<Integer> pavadinimas = new ArrayList<>();

        for (int i=0; i<10; i++){
            pavadinimas.add(3*i);
        }
        for (int i = 0; i <pavadinimas.size(); i++) {
            System.out.println(pavadinimas.get(i));
        }

        int inter=0;
        do{
            System.out.println(pavadinimas.get(inter));
            inter++;
        }while(inter<pavadinimas.size());

        while(inter<pavadinimas.size()){
            System.out.println(pavadinimas.get(inter));
            inter++;
        }

        for (Integer elment : pavadinimas) {
            System.out.println(elment);
        }

        List<String> tekstuSarasa = new ArrayList<>();
        tekstuSarasa.add("pirmas");
        tekstuSarasa.add("antras");
        tekstuSarasa.add("trecias");

        for (String tekstas:tekstuSarasa) {
            System.out.println(tekstas);
        }

    }
}
