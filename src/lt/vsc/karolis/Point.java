package lt.vsc.karolis;

public class Point {
    private int x=0;
    private int y=0;
    private String color;
    public double calculateDistance(int x, int y){
        int deltax = Math.abs(x-this.x);
        int deltay = Math.abs(y-this.y);
        double delta = Math.sqrt(deltax*deltax+deltay*deltay);
        return delta;
    }
    public int checkQarter(int x, int y){
        if(x>0&&y>0){
            return 1;
        }else if(x<0&&y>0){
            return 2;
        }else if(x<0&&y<0){
            return 3;
        }else if(x>0&&y<0){
            return 4;
        }else {
            return 0;
        }
    }

    public boolean chechkIfQarterIsSame(int x, int y){
        if(checkQarter(this.x, this.y)==checkQarter(x, y)){
            return true;
        }else {
            return false;
        }
    }

    public Point() {
    }
    public Point(int x, int y) {
        this.x=x;
        this.y=y;
    }
    public Point(int x, int y, String color) {
        this.x=x;
        this.y=y;
        this.color=color;
    }
    public Point(String color){
        this.color=color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (x != point.x) return false;
        if (y != point.y) return false;
        return color != null ? color.equals(point.color) : point.color == null;
    }


    public static void main(String[] args) {
        Point firstPoint = new Point(10,10, "red");
        System.out.println(firstPoint.checkQarter(-15,10));
        System.out.println(firstPoint.calculateDistance(11,-3));
        System.out.println(firstPoint.chechkIfQarterIsSame(12,1));

    }
}
