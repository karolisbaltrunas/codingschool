package lt.vsc.karolis.desimtKlasiu;

public class Pirma {
    //sukuriu Antra tipo antra objekta
    private Antra antrasObjektas;
    //sukuriu konstruktori, nes cia objektinis proramavimas
    public Pirma(){
        antrasObjektas = new Antra();
    }
    //sukuriu ketoda kuris kreipiasi i antra klase
    public int liepkRekt7Objektui(){
       return antrasObjektas.liepkRekt7Objektui();
    }
    public Fantas imkFanta(Fantas fantas){
        antrasObjektas.imkFanta(fantas);
        return fantas;
    }
}
