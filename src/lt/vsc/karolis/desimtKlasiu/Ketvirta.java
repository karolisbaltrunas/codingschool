package lt.vsc.karolis.desimtKlasiu;

public class Ketvirta {
    private Penkta penktasObjektas;
    public Ketvirta (){
        penktasObjektas = new Penkta();
    }
    public int liepkRekt7Objektui() {
       return penktasObjektas.liepkRekt7Objektui();
    }

    public Fantas imkFanta(Fantas fantas) {
        penktasObjektas.imkFanta(fantas);
        return fantas;
    }
}
