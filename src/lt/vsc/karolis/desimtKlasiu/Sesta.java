package lt.vsc.karolis.desimtKlasiu;

public class Sesta {
    private Septinta septintasObjektas;
    public Sesta (){
        septintasObjektas = new Septinta();
    }
    public int liepkRekt7Objektui() {
        return septintasObjektas.rekti();
    }

    public Fantas imkFanta(Fantas fantas) {
        septintasObjektas.imkFanta(fantas);
        return fantas;
    }
}
