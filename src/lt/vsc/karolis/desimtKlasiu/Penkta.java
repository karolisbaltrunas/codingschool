package lt.vsc.karolis.desimtKlasiu;

public class Penkta {
    private Sesta sestasObjektas;
    public Penkta (){
        sestasObjektas = new Sesta();
    }
    public int liepkRekt7Objektui() {
        return sestasObjektas.liepkRekt7Objektui();
    }

    public Fantas imkFanta(Fantas fantas) {
        sestasObjektas.imkFanta(fantas);
        return fantas;
    }
}
