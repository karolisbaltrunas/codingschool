package lt.vsc.karolis.desimtKlasiu;

public class Antra {
    private Trecia treciasObjektas;
    public Antra (){
        treciasObjektas = new Trecia();
    }
    public int liepkRekt7Objektui() {
       return treciasObjektas.liepkRekt7Objektui();
    }

    public Fantas imkFanta(Fantas fantas) {
        treciasObjektas.imkFanta(fantas);
        return fantas;
    }
}
