package lt.vsc.karolis.desimtKlasiu;

public class Trecia {
    private Ketvirta ketvirtasObjektas;
    public Trecia (){
        ketvirtasObjektas = new Ketvirta();
    }
    public int liepkRekt7Objektui() {
       return ketvirtasObjektas.liepkRekt7Objektui();
    }

    public Fantas imkFanta(Fantas fantas) {
        ketvirtasObjektas.imkFanta(fantas);
        return fantas;
    }
}
