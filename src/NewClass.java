import main.Student;

public class NewClass extends Student {
    public static void main(String[] args) {
        //Kai extends, tuomet protected yra galimas, bet is objekto kuris extendina.
        NewClass newClass = new NewClass();
        newClass.protectedMethod();

        Student karolis = new Student();
        karolis.publicMethod();
//        karolis.modifier();
//        karolis.privatus();
//        karolis.protectedMethod();

        /////////if ir switch ////////
        String word = "labas";

        if (word.equals("zodis")){
            System.out.println(word);
        }else if(word.equals("labas")){
            System.out.println(word);
        }else if(word.equals("nelabas")){
            System.out.println(word);
        }else{
            System.out.println("nerasta");
        }

        switch (word){
            case "labas":
                System.out.println(word);
                System.out.println("bla bla");
                break;
            case "velnias":
                System.out.println(word);
                break;
            case "nelabas":
                System.out.println(word);
                break;
            default:
                System.out.println("nerasta");
                break;
        }

        int skaicius = 5;
        switch (skaicius){
            case 3:
                break;
            case 2:
                System.out.println("labas rytas");
                System.out.println("teisingai");
                break;
            case 5:
                System.out.println("teisingai cia 5");
                break;
            default:
                System.out.println("neradom");
                break;
        }
    }
}
